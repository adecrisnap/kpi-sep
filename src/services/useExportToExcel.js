import React, {useEffect, useState} from 'react'
import * as FileSaver from "file-saver"
import * as XLSX from "xlsx"
import Helper from './Helper'

const useExportToExcel = (data, fileName) => {
  const [apiData, setApiData] = useState(data)
  const [fName, setFileName ] = useState(fileName)

  const [exportRefresh, setExportRefresh] = useState(null)
  const fileType =
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetcharset=UTF-8"
  const fileExtension = ".xlsx"

  const setExportToExcel = () =>{
    setExportRefresh(Helper.makeid())
  }
  
  useEffect(() => {   
    const exportToCSV = (apiData, fileName) => {
      const ws = XLSX.utils.json_to_sheet(apiData)
      const wb = { Sheets: { data: ws }, SheetNames: ["data"] }
      const excelBuffer = XLSX.write(wb, { bookType: "xlsx", type: "array" })
      const data = new Blob([excelBuffer], { type: fileType })
      FileSaver.saveAs(data, fileName + fileExtension)
    }
    if (exportRefresh!==null)
      exportToCSV(apiData,fName)
  }, [exportRefresh])
  
  return { setApiData, setFileName, setExportToExcel }
}

export default useExportToExcel