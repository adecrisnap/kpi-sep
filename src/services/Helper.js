class helper {
    formatDate(dtd) {
        var dtx = null;
        if (dtd) {
            var dt = new Date(dtd);
            var dtm = (dt.getMonth()+1).toString();
            var dtt = dt.getDate().toString();
            if (dtt.length===1) dtt = '0' + dtt;
            if (dtm.length===1) dtm = '0' + dtm;
            var hours = dt.getHours();
            var minutes = dt.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            hours = hours < 10 ? '0' +hours : hours;
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            dtx = dt.getFullYear().toString() + '-' + dtm + '-' + dtt + ' ' + strTime;
        }
        return dtx;
    }

    makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 8; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    findIndex(jsonObject,id,value) {
        var found = false;
        var idx = -1;
        var i = 0;
        while (found===false && i<jsonObject.length){
            if (jsonObject[i][id]===value) {
                found = true;
                idx = i;
            }
            i++;
        }
        return idx;
    }

    filterByValue(array, string) {
        return array.filter(o =>
            Object.keys(o).some(k => o[k].toLowerCase().includes(string.toLowerCase())));
    }

    filterById(jsonObject, id, value) {
        return jsonObject.filter(
            function(jsonObject) {
                return (jsonObject[id] == value);
            }
        )[0];
    }

    filterArray(array, filters) {
        const filterKeys = Object.keys(filters);
        return array.filter(item => {
          // validates all filter criteria
          return filterKeys.every(key => {
            // ignores non-function predicates
            if (typeof filters[key] !== 'function') return true;
            return filters[key](item[key]);
          });
        });
    }

    formatDateShort(dtd) {
        var dtx = null;
        if (dtd) {
            var dt = new Date(dtd);
            var dtm = (dt.getMonth()+1).toString();
            var dtt = dt.getDate().toString();
            if (dtt.length===1) dtt = '0' + dtt;
            if (dtm.length===1) dtm = '0' + dtm;
            dtx = dt.getFullYear().toString() + '-' + dtm + '-' + dtt;
        }
        return dtx;
    }

    /*
        getImageUrl() {
            return 'https://pdx.co.id:1338/dxl.jpg';
        }

        getDefaultUserURL() {
            return '../img/user.png';
        }
    */  
}

const Helper = new helper();  
export default Helper;