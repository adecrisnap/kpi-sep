import { useState, useEffect, useReducer } from 'react'
import Helper from './Helper';

const dataFetchReducer =(stateDelete, action) => {
    switch (action.action.type) {
      case 'FETCH_INIT':
        return {
          ...stateDelete,
          isLoading: true,
          hasError: false
        };
      case 'FETCH_SUCCESS':
        return {
          ...stateDelete,
          isLoading: false,
          hasError: false,
          data: action.action.data,
          status : action.action.status,
          randomstatus : action.action.randomstatus,
        };
      case 'FETCH_FAILURE':
        return {
          ...stateDelete,
          isLoading: false,
          hasError: true,
          errorMessage : action.action.errorMessage,
          status : action.action.status,
          randomstatus : action.action.randomstatus,
        };
      default:
        throw new Error();
    }
};

const useDeleteData = (initialUrl,token) => {
    const [url, setDeleteUrl] = useState(initialUrl)
    const [refresh, setRefresh] = useState(false)
    const [stateDelete, dispatch] = useReducer(dataFetchReducer, {
        isLoading: false,
        hasError: false,
        data: null,
        errorMessage : '',
        status : 0,
        randomtstatus : 0,
 
    });
    useEffect(() => {   
        const fetchData = () => {
            var action = 
            { 
              type : 'FETCH_INIT',
              errorMessage : '',
              status : 0,
              randomstatus : 0,
              data : null
            }
            dispatch({action});
            var pload = {}
            if (token!=='') 
              pload =  {
                method: "DELETE",
                headers: {
                    'Authorization' : token,
                    'Content-Type' : 'application/json',
                },
                
            }
            else
              pload =  {
                  method: "DELETE",
                  headers: {
                      'Content-Type' : 'application/json',
                  },
                  
              }
            return fetch(url, pload
            )
            .then(r =>  r.json().then((data) => {
                if (r.status!==200 && r.status!==201 && r.status!==304) {
                    action = { type: 'FETCH_FAILURE' , errorMessage : 'Error', status : r.status, data:null,
                      randomstatus : Helper.makeid()
                    };
                    dispatch({action});
                }
                else {
                 
                    action = 
                    { 
                      type: 'FETCH_SUCCESS', 
                      data: data, 
                      status : r.status, 
                      randomstatus : Helper.makeid(),
                      errorMessage : '',
                    };
                    dispatch({action});
                }
            }))
        }
        if (url!==null && url!=='') {
          if (refresh===true || stateDelete.data===null) fetchData()
        }
    }, [url,refresh])
    return { stateDelete, setDeleteUrl, setRefresh }
}

export default useDeleteData