import React, { useReducer, useEffect, useState } from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import { AuthContext } from './services/stores/AuthContext';
import { initialState } from './services/stores/initialState';
import { reducer } from './services/stores/reducer';
import { BrowserRouter } from 'react-router-dom';
import Home from './Home';
import Login from './Login'
import Cookies from 'js-cookie'
import queryString from 'query-string';



function App() {
  var token = null
  let params = queryString.parse(window.location.search)
  
  var { 
    kpitoken
  } = params;

  if (kpitoken!==null && kpitoken!==undefined) token = kpitoken

  const [stateContext, dispatch] = useReducer(reducer, initialState);
  //const [ storedData ] = useState(JSON.parse(localStorage.getItem('kpiData')));
  return (
      <BrowserRouter>
          <AuthContext.Provider
            value={{
              stateContext,
              dispatch
            }}
          >
            {(()=> {
                  
                  if (token!==null && token!==undefined) {
                    return  <div className="App">
                              <Home storedData={token} fromLocal={true}/>
                            </div>
                  }
                  else {
                    return  <div className="App">
                              <Login/>
                            </div>
                  }  
                })()}
          </AuthContext.Provider>
      </BrowserRouter>
    );
}

export default App;
/*

 {(()=> {
                  if (token!==null && token!==undefined) {
                    return  <div className="App">
                              <Home storedData={token} fromLocal={true}/>
                            </div>
                  }
                  else {
                    return  <div className="App">
                              <Login/>
                            </div>
                  }  
                })()}

                */