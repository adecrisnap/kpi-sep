import React, { useEffect, useState } from 'react';
import { 
    Container,
} from 'semantic-ui-react';
import LoadingBox from '../components/LoadingBox';
import useGetData from '../services/useGetData';
import GridTable from '../components/GridTable'
import URLLocation from '../services/URLLocation';
import HeaderEnhanced from '../components/HeaderEnhanced';
import {NotificationContainer, NotificationManager} from 'react-notifications';


const UserManagementList_backup = props => {
    const [dataSource,setDataSource] = useState(null);
    const [columns,setColumns] = useState(null);
    const  { stateGet,setGetUrl } = useGetData(
        null,''
    );

    useEffect(()=>{
        setGetUrl(URLLocation.getUrl() + '/getroleskpi');
    },[])

    useEffect(()=>{
        if (stateGet.status===200 || stateGet.status===201 || stateGet.status===304) {
            var columns = [];
            columns.push({columnname : 'id', columncaption : '', dataType : 'Button',  width : 2, action : 'edit', buttonCaption : 'Edit', visible : true})
            columns.push({columnname : 'id', columncaption : '', dataType : 'Button',  width : 2, action : 'delete', buttonCaption : 'Delete', visible : true})
            columns.push({columnname : 'id', columncaption : ' ', dataType : 'String', width : 5, link : 'rolekpi/edit', visible : false})
            columns.push({columnname : 'role_name', columncaption : 'Role Name', dataType: 'String', width: 8});
            columns.push({columnname : 'lvl', columncaption : 'Level', dataType: 'Custom', width: 8, customvalues : [ {value : 2, text : 'Divisi'}, {value: 3, text :'Department'}]});
            setColumns(columns);
            setDataSource(stateGet.data);
        }
        else {
            var msg = '';
            msg = stateGet.errorMessage;
            if (msg!=='') NotificationManager.error(msg, 'Error', 3000);
        }
    },[stateGet.randomstatus])

    return (
        <Container>
            <NotificationContainer/>
            <HeaderEnhanced title='Daftar Roles' text='Adalah semua roles di organisasi' />
            {(stateGet.isLoading===true) ?
                (
                    <LoadingBox open={true}/>
                )
                :
                (
                    <GridTable
                        datasource={dataSource} 
                        columns={columns}
                        rowsperpage={10}
                        sortedby={'seq1'}
                        linkinrowlevel={true}
                        ascdesc={'ascending'}
                        showaddnew={true}
                        //link={'/rolesrolesdetail/add/' +roleid}
                        //buttonClick={buttonClick}
                        addnewbuttoncaption='Add new'
                        showaddnewonempty={true}
                        emptycaption="Anda tidak memiliki roles"
                        emptyimage={null}
                        emptytitle="Tidak ada Roles"
                        emptybuttoncaption="Add new role"
                    />
                )
            }
        </Container>
    )
}

export default UserManagementList_backup;
