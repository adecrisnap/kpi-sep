import EmptySpace from '../components/EmptySpace'

const ErrorAccess = () => {
    return (
        <EmptySpace
            title={'Anda tidak memiliki akses ke menu ini'}
            caption={'Silakan kontak administrator untuk mendapatkan hak akses'}
            showButton={false}
            image={null}
            buttonCaption={''}
            link={null}
        />
    )
}

export default ErrorAccess