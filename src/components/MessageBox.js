import React from 'react';
import { Modal, Button } from 'semantic-ui-react';
//import {  Dimmer, Loader, Modal } from 'semantic-ui-react';

const MessageBox = props => {
    return (
        <Modal
            trigger={<Button>Show Modal</Button>}
            header='Info'
            content={props.message}
            actions={[ { key: 'done', content: 'Done', positive: true }]}
        />
    );    
}

export default MessageBox


/*
  <Modal size={'mini'} open={props.open}>
            <Modal.Header>
                <Modal.Title>Info</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>{props.message}</p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={props.onClose}>OK</Button>
            </Modal.Footer>
        </Modal>
*/