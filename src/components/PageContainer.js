import React, { useEffect } from 'react';
import { Route,Switch, Link } from 'react-router-dom';
//import PrivateRoute from './PrivateRoute';
import KPILevel1 from '../pages/kpilevel1'
import KPILevel2 from '../pages/kpilevel2'
import KPILevel3 from '../pages/kpilevel3'
import KPILevel1Detail from '../pages/kpilevel1detail'
import KPILevel2Detail from '../pages/kpilevel2detail'
import KPILevel3Detail from '../pages/kpilevel3detail'
import Konfigurasi from '../pages/konfigurasi'
import Target from '../pages/target'
import TargetDetail from '../pages/targetdetail'
import KonfigurasiDetail from '../pages/konfigurasidetail'
import EditKPIPage1 from '../pages/EditKPIPage1'
import HitungKPI from '../pages/HitungKPI'
import Login from '../Login'
import Home from '../HomeBackup'
import ErrorAccess from '../pages/ErrorAccess'
import UserManagementList from '../pages/userManagementList';
import UserManagement from '../pages/userManagement';
import RolesRolesDetail from '../pages/rolesrolesdetail';
import { useLocation } from 'react-router';

const PageContainer = props => {
      const location = useLocation()
      return (
            <Switch>
                  <Route exact path='/kpilevel1' key={location.key} component={KPILevel1} />
                  <Route exact path='/kpilevel2' key={location.key} component={KPILevel2} />
                  <Route exact path='/kpilevel3' key={location.key} component={KPILevel3} />
                  <Route exact path='/kpilevel1detail/:mode/:id' key={location.key} component={KPILevel1Detail} />
                  <Route exact path='/kpilevel2detail/:mode/:id' key={location.key} component={KPILevel2Detail} />
                  <Route exact path='/kpilevel3detail/:mode/:id' key={location.key} component={KPILevel3Detail} />
                  <Route exact path='/konfigurasi/' key={location.key} component={Konfigurasi} />
                  <Route exact path='/target/' key={location.key} component={Target} />
                  <Route exact path='/targetdetail/' key={location.key} component={TargetDetail} />
                  <Route exact path='/editscore/' key={location.key} component={EditKPIPage1} />
                  <Route exact path='/konfigurasidetail/' key={location.key} component={KonfigurasiDetail} />
                  <Route exact path='/hitungkpi/' key={location.key} component={HitungKPI}/>
                  <Route exact path='/login' key={location.key} component={Login}/>
                  <Route exact path='/home' key={location.key} component={EditKPIPage1}/>
                  <Route exact path='/erroraccess' key={location.key} component={ErrorAccess}/>
                  <Route exact path='/usermanagement/' key={location.key} component={UserManagementList}/>
                  <Route exact path='/rolesrolesdetail/:mode/:roleid' key={location.key} component={RolesRolesDetail}/>
                  <Route exact path='/rolekpi/:mode/:roleid' key={location.key} component={UserManagement}/>
                  <Route path='/quicksight' component={() => { 
                        //window.location.href = 'https://us-east-1.signin.aws.amazon.com/oauth?SignatureVersion=4&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIYOJP4OBNT7XFVSQ&X-Amz-Date=2022-07-07T06%3A29%3A14.659Z&X-Amz-Signature=f34aebb22a84f350cbe8b7f64b2b20245dc48f90c6d6192e96f93b063d32762e&X-Amz-SignedHeaders=host&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Fspaceneedle-prod&isauthcode=true&qs-signin-user-auth=false&response_type=code&state=hashArgs%23&redirect_uri=https%3A%2F%2Fus-east-1.quicksight.aws.amazon.com%2Fsn%2Fstart%3Fqs-signin-user-auth%3Dfalse%26state%3DhashArgs%2523%26isauthcode%3Dtrue&qs-signin-account-name=sepadmin&directory_alias=sepadmin&rdfs=true'; 
                        //window.open('https://us-east-1.signin.aws.amazon.com/oauth?SignatureVersion=4&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIYOJP4OBNT7XFVSQ&X-Amz-Date=2022-07-07T06%3A29%3A14.659Z&X-Amz-Signature=f34aebb22a84f350cbe8b7f64b2b20245dc48f90c6d6192e96f93b063d32762e&X-Amz-SignedHeaders=host&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Fspaceneedle-prod&isauthcode=true&qs-signin-user-auth=false&response_type=code&state=hashArgs%23&redirect_uri=https%3A%2F%2Fus-east-1.quicksight.aws.amazon.com%2Fsn%2Fstart%3Fqs-signin-user-auth%3Dfalse%26state%3DhashArgs%2523%26isauthcode%3Dtrue&qs-signin-account-name=sepadmin&directory_alias=sepadmin&rdfs=true', '_blank');
                        window.open('https://ap-southeast-1.signin.aws.amazon.com/oauth?SignatureVersion=4&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIYOJP4OBNT7XFVSQ&X-Amz-Date=2022-07-07T06%3A29%3A14.659Z&X-Amz-Signature=f34aebb22a84f350cbe8b7f64b2b20245dc48f90c6d6192e96f93b063d32762e&X-Amz-SignedHeaders=host&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Fspaceneedle-prod&isauthcode=true&qs-signin-user-auth=false&response_type=code&state=hashArgs%23&redirect_uri=https%3A%2F%2Fus-east-1.quicksight.aws.amazon.com%2Fsn%2Fstart%3Fqs-signin-user-auth%3Dfalse%26state%3DhashArgs%2523%26isauthcode%3Dtrue&qs-signin-account-name=sepadmin&directory_alias=sepadmin&rdfs=true', '_blank');
                        return null;
                  }}/>

            </Switch>
      );

}

// Redirect Page
const RedirectPage = () => {
      useEffect(() => {
        window.location.replace('https://us-east-1.signin.aws.amazon.com/oauth?SignatureVersion=4&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIYOJP4OBNT7XFVSQ&X-Amz-Date=2022-07-07T06%3A29%3A14.659Z&X-Amz-Signature=f34aebb22a84f350cbe8b7f64b2b20245dc48f90c6d6192e96f93b063d32762e&X-Amz-SignedHeaders=host&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Fspaceneedle-prod&isauthcode=true&qs-signin-user-auth=false&response_type=code&state=hashArgs%23&redirect_uri=https%3A%2F%2Fus-east-1.quicksight.aws.amazon.com%2Fsn%2Fstart%3Fqs-signin-user-auth%3Dfalse%26state%3DhashArgs%2523%26isauthcode%3Dtrue&qs-signin-account-name=sepadmin&directory_alias=sepadmin&rdfs=true');
      }, [])
    
      // Render some text when redirecting
      // You can use a loading gif or something like that
      return 
        <h3>Redirecting...</h3>
      
    }

export default PageContainer
