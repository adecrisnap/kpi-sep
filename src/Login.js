import React, { useEffect} from 'react';
import { 
  Container
} from 'semantic-ui-react';
import LoadingBox from './components/LoadingBox';
import URLLocation from './services/URLLocation';


const Login = () => {
  
  useEffect(()=>{
    window.location.href = URLLocation.getUrlAuth() + '/?original_appid=2'
  },[])

  return (
    <Container>
      <LoadingBox/>   
    </Container>
  );
}

export default Login;